package digital.amaranth.mc.firerealism;

import digital.amaranth.mc.quickblocklib.Neighbors.Neighbors;

import org.bukkit.Material;
import org.bukkit.block.Block;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import org.bukkit.event.block.BlockIgniteEvent;

public class FireSpread implements Listener {
    public FireSpread () {
    }
    
    private void igniteNeighbors (Block b) {
        Neighbors.getDirectNeighbors(b).stream().
                filter(n -> n.getType().isAir()).
                forEach(n -> n.setType(Material.FIRE));
    }
    
    @EventHandler
    public void onBlockIgniteEvent (BlockIgniteEvent e) {
        if (e.getPlayer() == null) {
            igniteNeighbors(e.getBlock());
        }
    }
}
