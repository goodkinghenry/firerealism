package digital.amaranth.mc.firerealism;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import org.bukkit.event.weather.LightningStrikeEvent;

public class LightningStrike implements Listener {
    private final double LIGHTNING_EXPLOSION_POWER;
    private final boolean lightningCausesFires;
    
    public LightningStrike () {
        this.LIGHTNING_EXPLOSION_POWER = 3;
        this.lightningCausesFires = true;
    }
    
    @EventHandler
    public void onLightningStrikeEvent (LightningStrikeEvent e) {
        e.getWorld().createExplosion(
                e.getLightning().getLocation(),
                (float)LIGHTNING_EXPLOSION_POWER,
                lightningCausesFires
        );
    }
}
