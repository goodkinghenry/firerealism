package digital.amaranth.mc.firerealism;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ExplosionPrimeEvent;

public class FieryExplosions implements Listener {
    @EventHandler (ignoreCancelled = true)
    public void onExplosionPrimeEvent (ExplosionPrimeEvent e) {
        e.setFire(true);
    }
}
