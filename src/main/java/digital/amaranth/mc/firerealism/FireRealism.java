package digital.amaranth.mc.firerealism;

import org.bukkit.Bukkit;

import org.bukkit.event.HandlerList;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class FireRealism extends JavaPlugin {
    public static FireRealism getInstance() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("FireRealism");
        if (plugin == null || !(plugin instanceof FireRealism)) {
            throw new RuntimeException("'FireRealism' not found.");
        }
        
        return ((FireRealism) plugin);
    }
    
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new LightningStrike(), this);
        getServer().getPluginManager().registerEvents(new FireSpread(), this);
        getServer().getPluginManager().registerEvents(new FieryExplosions(), this);
    }
    
    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
