This plugin for Spigot API 1.19 servers makes fire and lightning much more aggressive and dangerous. Fires will spread quickly, lightning bolts create an explosive effect, and explosions will start fires as well.

Current version: 0.2
